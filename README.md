## Parker Berberian's Budgeting App
This is Parker Berberian's submission for IT612 assignment 5.


### About
This is a simple budgeting app. On the left, you will see a list where you can keep track of monthly expenses. A few are prepopulated for you, but you can create or delete any expense. The app will keep track of all your monthyl expenses and tell you how much money per month you are spending.
You are also able to input your income information in the center column. You can input either your yearly or monthly income, and the other field will be calculated for you. Your after tax income is estimated assuming you use the standard deduction and no other exemptions.
In the right column, you are given a brief summary about your financial health based on the values you have provided.
