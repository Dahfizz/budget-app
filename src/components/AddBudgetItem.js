import React, {useState} from 'react';


function AddBudgetItem(props){
    const [name, setName] = useState("");
    const [cost, setCost] = useState("");
    return (
        <div className="add-budget">
            <h4>Add a budget item</h4>

        <div className="right-text">
            <div className="input-wrapper">
            <label>Name: </label>
            <input value={name} onChange={(e)=>setName(e.target.value)}/>
            </div>

            <div className="input-wrapper">
            <label>Cost: </label>
            <input value={cost} onChange={(e)=> setCost(e.target.value)}/>
            </div>

            <button className="btn" onClick={()=>{
                props.callback(name, cost);
                setName("");
                setCost(0);
            }}>Add</button>
        </div>
        </div>
    );
}


export default AddBudgetItem;
