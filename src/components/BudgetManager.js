import React from 'react';
import BudgetItem from './BudgetItem';
import AddBudgetItem from './AddBudgetItem';
import Summary from './Summary';


const default_items = [
    {"name": "Rent", "cost": 1500},
    {"name": "Car Payment", "cost": 300},
    {"name": "Utilities", "cost": 150},
]

const tax_brackets = {
    "single": [
        {"rate": 0.10, "range": [0,      9700]},
        {"rate": 0.12, "range": [9701,   39475]},
        {"rate": 0.22, "range": [39476,  84200]},
        {"rate": 0.24, "range": [84201,  160725]},
        {"rate": 0.32, "range": [160726, 204100]},
        {"rate": 0.35, "range": [204101, 510300]},
        {"rate": 0.37, "range": [510301, Number.POSITIVE_INFINITY]},
    ],
    "married, seperate": [
        {"rate": 0.10, "range": [0,      9700]},
        {"rate": 0.12, "range": [9701,   39475]},
        {"rate": 0.22, "range": [39476,  84200]},
        {"rate": 0.24, "range": [84201,  160725]},
        {"rate": 0.32, "range": [160726, 204100]},
        {"rate": 0.35, "range": [204101, 306750]},
        {"rate": 0.37, "range": [306751, Number.POSITIVE_INFINITY]},
    ],
    "married, joint": [
        {"rate": 0.10, "range": [0,      19400]},
        {"rate": 0.12, "range": [19401,  78950]},
        {"rate": 0.22, "range": [78951,  168400]},
        {"rate": 0.24, "range": [168401, 321450]},
        {"rate": 0.32, "range": [321451, 408200]},
        {"rate": 0.35, "range": [408201, 612350]},
        {"rate": 0.37, "range": [612351, Number.POSITIVE_INFINITY]},
    ],
    "head of household": [
        {"rate": 0.10, "range": [0,      13850]},
        {"rate": 0.12, "range": [13851,  52850]},
        {"rate": 0.22, "range": [52851,  84200]},
        {"rate": 0.24, "range": [84201,  160725]},
        {"rate": 0.32, "range": [160726, 204100]},
        {"rate": 0.35, "range": [204101, 510300]},
        {"rate": 0.37, "range": [510301, Number.POSITIVE_INFINITY]},
    ],
}

function after_tax(income, status){
    let after_deductions = income - 12200;
    let rates = tax_brackets[status];
    let parts = [0]
    for (const rate of rates){
        const spread = rate.range[1] - rate.range[0];
        let part = after_deductions - rate.range[0];
        if (part < 1)
            break;
        if (part > spread)
            part = spread;
        parts.push(part * rate.rate);
    }

    const tax = parts.reduce( (a, v)=> a + v, 0);
    return (income -  tax).toFixed(2);
}

class BudgetManager extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            costs: default_items,
            income: 0,
            filing: "single"
        }
    }

    removeItem(i){
        this.setState((state, props) => {
          return {costs: state.costs.slice(0,i).concat(state.costs.slice(i+1))};
        });
    }

    addItem(name, c){
        const cost = Number(c);
        if(!name || Number.isNaN(cost))
            return;
        this.setState((state, props) => {
          return {
              costs: state.costs.slice(0).concat([{"name": name, "cost": cost}])
          }
        });
    }

    monthlyIncome(){
        const incm = this.state.income;
        return Number(Number.isInteger(incm)?incm:incm.toFixed(2));
    }

    yearlyIncome(){
        const incm = this.state.income * 12;
        return Number(Number.isInteger(incm)?incm:incm.toFixed(2));
    }

    monthlyCost(){
        let cost = this.state.costs.reduce((a, v)=>a+v.cost, 0)
        return Number(Number.isInteger(cost)?cost:cost.toFixed(2));
    }


    render(){
        return (
        <div id="budget-app-container">
            <div className="outer-block" id="monthly">
                <h2>Monthly Expenses</h2>
                <div id="budget-items">
                    {this.state.costs.map((v, i)=>
                        <BudgetItem key={i} name={v.name} value={v.cost} onDelete={()=>this.removeItem(i)} /> )}
                </div>
                <AddBudgetItem callback={(name, cost)=>this.addItem(name, cost)}/>
                <h2>{`Total Monthly Expense: ${this.monthlyCost()}`}</h2>
            </div>

            <div className="outer-block" id="income">
                <h2>Income</h2>
                <div className="right-text">
                    <div className="input-wrapper">
                        <label>Annual Income: </label>
                        <input type="text" value={this.yearlyIncome()}
                            onChange={(e)=>this.setState({income: Number(e.target.value)/12})}/>
                    </div>
                    <div className="input-wrapper">
                        <label>Monthly Income: </label>
                        <input type="text" value={this.monthlyIncome()}
                            onChange={(e)=>this.setState({income: Number(e.target.value)})}/>
                    </div>

                    <div className="input-wrapper">
                        <label>Tax Status: </label>
                    <select id="taxSelect" onChange={(e)=>this.setState({filing: e.target.value})}>
                        <option value="single">Single</option>
                        <option value="married, seperate">Married, Seperate</option>
                        <option value="married, joint">Married, Joint</option>
                        <option value="head of household">Head of Household</option>
                    </select>
                    </div>
                </div>
                <h3>{`Estimated After Tax Income: ${after_tax(this.state.income*12, this.state.filing)}`}</h3>
            </div>

            <div className="outer-block" id="summary">
                <Summary income={this.state.income} after_tax={after_tax(this.state.income*12, this.state.filing)} budget={this.monthlyCost()} />
            </div>

        </div>
        )
    }
}


export default BudgetManager;
