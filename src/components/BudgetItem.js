import React from 'react';


function BudgetItem(props){
    return (
        <div className="budget-item">
        {props.fixed?'':<button className="delete-btn" onClick={()=> props.onDelete()}>X</button>}
            <div className="budget-text-wrapper">
                <p className="budget-item-name">{`${props.name}: `}</p>
                <p className="budget-item-value">{`${props.value}`}</p>
            </div>
        </div>
    );
}


export default BudgetItem;
