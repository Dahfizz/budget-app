import React from 'react';
import BudgetItem from './BudgetItem';


function Summary(props){

    return (
        <div className="add-budget">
            <h2>Summary</h2>
            <BudgetItem fixed="1" name="Monthly Expenses" value={props.budget.toFixed(2)} />
            <BudgetItem fixed="1" name="Monthly After-Tax Income" value={(props.after_tax / 12).toFixed(2)} />
            <BudgetItem fixed="1" name="Monthly Net" value={((props.after_tax / 12) - props.budget).toFixed(2)} />
            <BudgetItem fixed="1" name="Annual Net" value={(props.after_tax - (props.budget*12)).toFixed(2)} />
        </div>
    );
}


export default Summary;
