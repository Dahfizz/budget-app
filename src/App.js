import React from 'react';
import './App.css';
import BudgetManager from './components/BudgetManager';

function App() {
  return (
        <div id="App">
            <h1>A Simple Budgeting App, By Parker Berberian</h1>
            <h3> More information <a href="https://gitlab.com/Dahfizz/budget-app/-/blob/master/README.md">here</a></h3>
            <BudgetManager/>
        </div>
  );
}

export default App;
